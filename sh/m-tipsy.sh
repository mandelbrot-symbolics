#!/bin/bash
lo=".(0011)"
hi=".(0100)"
sharpness="16"
for tip in ".001(0)" ".011(0)" ".101(0)" ".111(0)"
do
  m-tipsy "${lo}" "${hi}" "${tip}" |
  tail -n 1 |
  (
    read period ray
    steps="$(( 2 * period * sharpness ))"
    precision="$(( 2 * period ))"
    m-exray-in "${precision}" "${ray}" "${sharpness}" "${steps}" |
    (
      read gre gim
      m-nucleus "${precision}" "${gre}" "${gim}" "${period}" 64 1 |
      (
        read nre nim
        m-size "${precision}" "${nre}" "${nim}" "${period}" |
        (
          read size angle
          emndl.sh --re "${nre}" --im "${nim}" --size "${size}" --period "${period}" --quality 9 --png --mkv
        )
      )
    )
  )
done
ls -1 emndl_*/out.mkv |
tail -n 4 |
tr "\n" " " |
(
  read a b c d
  ffmpeg -i "${a}" -i "${b}" -i "${c}" -i "${d}" \
    -filter_complex "[1:v][0:v]hstack[h0];[2:v][3:v]hstack[h1];[h0][h1]vstack" \
    -pix_fmt yuv420p -profile:v high -level:v 4.1 -crf:v 20 -an \
    -movflags +faststart \
    m-tipsy.mp4
)
