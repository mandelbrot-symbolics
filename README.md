mandelbrot-symbolics
====================

Symbolic algorithms related to the Mandelbrot set: computations on external
angles, kneading sequences, (angled) internal addresses, etc.
