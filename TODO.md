TODO
====

AngledAddress
-------------

Port from 'ExternalAngle' to 'BinaryAngle'.

Use 'bulb' instead of 'wakees' where appropriate.

Move and rename 'wakees' to 'lavaurs' in a new module 'Lavaurs'.

Add 'ConciseAddress' and operations.


ExternalAngle
-------------

Optimize 'Ord' instance for 'BinaryAngle' and make it safe for non-canonical
forms.

Add 'otherRepresentation' for ...1(0) -> ...0(1) with special case for .(0).


InternalAngle
-------------

Add 'bulb' and friends.


Misiurewicz
-----------

Add navigating by spokes.


Parse
-----

Ensure base 10 digits where appropriate.

Validation and canonicalization of results.


Rational
--------

Instances for 'Ratio': 'Num', 'Fractional', 'Real', 'RealFrac'.


Integer
-------

Optimize 'odd' and 'even' to bitops.

Optimize masking - try masking before 'shiftL' where appropriate.
