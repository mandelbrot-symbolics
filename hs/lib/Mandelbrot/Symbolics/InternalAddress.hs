module Mandelbrot.Symbolics.InternalAddress
  ( InternalAddress(..)
  , internalAddress'
  , internalAddress
  , associated'
  , associated
  , upper'
  , upper
  , lower'
  , lower
  ) where

import Mandelbrot.Symbolics.Block
  ( Block(..)
  , (!)
  , compact
  , singleton
  , toList
  , append
  )
import Mandelbrot.Symbolics.Kneading
  ( Kneading(..)
  )
import Mandelbrot.Symbolics.Period
  ( Period(periods)
  )

newtype InternalAddress = InternalAddress [Int]
  deriving (Read, Show, Eq, Ord)

instance Period InternalAddress where
  periods (InternalAddress xs) = (0, last xs)

-- | Construct an 'InternalAddress' from a kneading sequence.
internalAddress' :: Kneading -> Maybe InternalAddress
internalAddress' (StarPeriodic   (Block _ 0))         = Just . InternalAddress $ [1]
internalAddress' (StarPeriodic v@(Block _ n)) | v ! 0 = Just . InternalAddress $ address (n + 1) (unpack v ++ [Nothing])
internalAddress' (Periodic     v@(Block _ n)) | v ! 0 = Just . InternalAddress $ address n (unpack v)
internalAddress' _ = Nothing

internalAddress :: Kneading -> InternalAddress
internalAddress k = case internalAddress' k of
  Just a -> a
  Nothing -> error $ show ("internalAddress failed", k)

unpack :: Block -> [Maybe Bool]
unpack = map Just . toList

address :: Int -> [Maybe Bool] -> [Int]
address p v = takeWhile (<= p) $ address' v

address' :: [Maybe Bool] -> [Int]
address' v = address'' 1 [Just True]
  where
    address'' sk vk = sk : address'' sk' vk'
      where
        sk' = (1 +) . length . takeWhile id . zipWith (==) v . cycle $ vk
        vk' = take sk' (cycle v)


-- | A star-periodic kneading sequence's upper and lower associated
--   kneading sequences.
associated' :: Kneading -> Maybe (Kneading, Kneading)
associated' (StarPeriodic k@(Block _ n)) = do
  let a1 = compact $ k `append` singleton False
      a2 = compact $ k `append` singleton True
  InternalAddress xs <- internalAddress' (Periodic a2)
  let (a, abar) = if (n + 1) `elem` xs then (a2, a1) else (a1, a2)
  return (Periodic a, Periodic abar)
associated' _ = Nothing

associated :: Kneading -> (Kneading, Kneading)
associated k = case associated' k of
  Just kk -> kk
  Nothing -> error $ show ("associated failed", k)

-- | The upper associated kneading sequence.
upper' :: Kneading -> Maybe Kneading
upper' = fmap fst . associated'

upper :: Kneading -> Kneading
upper k = case upper' k of
  Just kk -> kk
  Nothing -> error $ show ("upper failed", k)

-- | The lower associated kneading sequence.
lower' :: Kneading -> Maybe Kneading
lower' = fmap snd . associated'

lower :: Kneading -> Kneading
lower k = case lower' k of
  Just kk -> kk
  Nothing -> error $ show ("lower failed", k)
