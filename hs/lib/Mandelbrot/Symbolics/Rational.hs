{-# LANGUAGE FlexibleInstances #-}

{- |
Rational numbers with operations useful in Mandelbrot set symbolic algorithms.
-}
module Mandelbrot.Symbolics.Rational
  ( Q(..)
  , Rational(..)
  ) where

import Prelude hiding (Rational)
import qualified Data.Ratio as Ratio

-- | Rational numbers with ruff-specific operations.
class Q r where
  {-# MINIMAL (%), numerator, denominator #-}

  infixl 7 %, %!

  -- | Safe constructor.  Reduces to canonical form.
  (%) :: Integer -> Integer -> r
  -- | Extract numerator.
  numerator :: r -> Integer
  -- | Extract denominator.
  denominator :: r -> Integer

  -- | Unsafe constructor.
  --   Precondition: numerator `gcd` denominator == 1 && denominator > 0
  {-# INLINE (%!) #-}
  (%!) :: Integer -> Integer -> r
  (%!) = (%)

  -- | 0.
  {-# INLINE zero #-}
  zero :: r
  zero = 0 %! 1

  -- | 1/2.
  {-# INLINE half #-}
  half :: r
  half = 1 %! 2

  -- | 1.
  {-# INLINE one #-}
  one  :: r
  one  = 1 %! 1

  -- | Convert to Prelude.Rational.
  {-# INLINE fromQ #-}
  fromQ :: r -> Ratio.Rational
  fromQ x = numerator x %! denominator x

  -- | Convert from Prelude.Rational.
  {-# INLINE toQ #-}
  toQ :: Ratio.Rational -> r
  toQ x = Ratio.numerator x %! Ratio.denominator x

  -- | Wrap into [0,1).
  {-# INLINE wrap #-}
  wrap :: r -> r
  wrap x = (numerator x `mod` denominator x) %! denominator x

  -- | Doubling map to [0,1).
  {-# INLINE doubleWrap #-}
  doubleWrap :: r -> r
  doubleWrap = {-# SCC "doubleWrap" #-} double . wrap

  -- | Doubling map from [0,1) to [0,1).
  --   Precondition: 0 <= x && x < 1
  {-# INLINE double #-}
  double :: r -> r
  double x = {-# SCC "double" #-} case () of
   _| even d    -> (if n < d' then n  else n - d') %  d'
    | otherwise -> (if n' < d then n' else n' - d) %! d
    where
      d = denominator x
      d' = d `div` 2
      n = numerator x
      n' = 2 * n

  -- | Doubling map from [0,1) to [0,1) for odd denominator.
  --   Precondition: 0 <= x && x < 1 && odd (denominator x)
  {-# INLINE doubleOdd #-}
  doubleOdd :: r -> r
  doubleOdd x = {-# SCC "doubleOdd" #-} (if n' < d then n' else n' - d) %! d
    where
      d = denominator x
      n = numerator x
      n' = 2 * n

  -- | Doubling map pre-images from [0,1) to [0,1)x[0,1).
  --   Precondition: 0 <= x && x < 1
  {-# INLINE preimages #-}
  preimages :: r -> (r, r)
  preimages x = (n % d', (n + d) % d')
    where
      n = numerator x
      d = denominator x
      d' = 2 * d


instance Q (Ratio.Ratio Integer) where
  {-# SPECIALIZE instance Q Ratio.Rational #-}
  {-# INLINE (%) #-}
  (%) = (Ratio.%)
  {-# INLINE numerator #-}
  numerator = Ratio.numerator
  {-# INLINE denominator #-}
  denominator = Ratio.denominator


-- | Rational data structure with exposed constructor for optimisations.
data Rational = !Integer :% !Integer deriving (Eq)

instance Q Rational where
  {-# SPECIALIZE instance Q Rational #-}
  {-# INLINE (%) #-}
  x % y = reduce (x * signum y) (abs y)
    where reduce x' y' = (x' `quot` d) :% (y' `quot` d) where d = gcd x' y'
  {-# INLINE (%!) #-}
  x %! y = x :% y
  {-# INLINE numerator #-}
  numerator (x :% _) = x
  {-# INLINE denominator #-}
  denominator (_ :% y) = y

instance Ord Rational where
  {-# SPECIALIZE instance Ord Rational #-}
  (x:%y) <= (x':%y') = x * y' <= x' * y
  (x:%y) <  (x':%y') = x * y' <  x' * y

instance Read Rational where
  readsPrec p = map (\(x,y) -> (toQ x, y)) . readsPrec p

instance Show Rational where
  showsPrec p = showsPrec p . fromQ
