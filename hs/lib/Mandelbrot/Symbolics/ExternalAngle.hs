{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{- |
External angles.
-}
module Mandelbrot.Symbolics.ExternalAngle
  ( ExternalAngle(..)
  , BinaryAngle(..)
  , binaryAngle
  , binary
  , binary2
  , rational
  , rational2
  , bits
  , Tuning(..)
  , tune2
  , otherRep'
  , otherRep
  ) where

import Prelude hiding
  ( Rational
  , concatMap
  , splitAt
  , take
  )
import Data.Bits
  ( shiftL
  , bit
  )

import Mandelbrot.Symbolics.Block
  ( Block(Block)
  , singleton
  , concatMap
  , compact
  , splitAt
  , take
  , toList
  , toListReversed
  , fromList
  , append
  )
import Mandelbrot.Symbolics.Period
  ( Period(periods, safePeriods)
  )
import Mandelbrot.Symbolics.Rational
  ( Q(..)
  , Rational
  )

newtype ExternalAngle = ExternalAngle Rational
  deriving (Read, Show, Eq, Ord)

instance Q ExternalAngle where
  n % d = ExternalAngle (n % d)
  n %! d = ExternalAngle (n %! d)
  numerator (ExternalAngle r) = numerator r
  denominator (ExternalAngle r) = denominator r

instance Period ExternalAngle where
  periods = periods . binary
  safePeriods maxPeriod = go 0
    where
      go n r
        | n > maxPeriod = Nothing
        | even (denominator r) = go (n + 1) (double r)
        | otherwise = go' 1 (doubleOdd r)
        where
          go' m r'
            | n + m > maxPeriod = Nothing
            | r' == r = Just (n, m)
            | otherwise = go' (m + 1) (doubleOdd r')

data BinaryAngle = BinaryAngle !Block !Block
  deriving (Eq, Read, Show)

instance Ord BinaryAngle where
  compare x y
    | x == y = EQ
    | otherwise = compare (bits x) (bits y)

instance Period BinaryAngle where
  periods (BinaryAngle (Block _ pp) (Block _ p)) = (pp, p)

-- | Tuning transformation for external angles.
--   Probably only valid for angle pairs representing hyperbolic components.
class Tuning t where
  tune :: (t, t) -> t -> t

tune2 :: Tuning t => (t, t) -> (t, t) -> (t, t)
tune2 p (a, b) = (tune p a, tune p b)

instance Tuning ExternalAngle where
  tune (lo, hi) t = rational (tune (binary lo, binary hi) (binary t))

instance Tuning BinaryAngle where
  tune (BinaryAngle _ lo, BinaryAngle _ hi) (BinaryAngle pre per)
    = binaryAngle (concatMap t pre) (concatMap t per)
    where
      t False = lo
      t True  = hi

binaryAngle :: Block -> Block -> BinaryAngle
binaryAngle pre@(Block _ pp) per@(Block _ p)
  = BinaryAngle pre' (compact (common `append` per'))
  where
    match
      = length . takeWhile id
      $ zipWith (==) (toListReversed pre) (toListReversed per)
    (pre', common) = splitAt (pp - match) pre
    per' = take (p - match) per

bits :: BinaryAngle -> [Bool]
bits (BinaryAngle pre per) = toList pre ++ cycle (toList per)

binary :: ExternalAngle -> BinaryAngle
binary a0 = (\(pp, p) -> BinaryAngle (fromList pp) (b p)) . binary' . wrap $ a0
  where
    b p = if a0 == one then singleton True else fromList p
    binary' a
      | even (denominator a) =
          let (pre, per) = binary' (double a)
          in  ((a >= half) : pre, per)
      | otherwise = ([], (a >= half) : binary'' (doubleOdd a))
      where
        binary'' a'
          | a' == a = []
          | otherwise = (a' >= half) : binary'' (doubleOdd a')

binary2 :: (ExternalAngle, ExternalAngle) -> (BinaryAngle, BinaryAngle)
binary2 (lo, hi) = (binary lo, binary hi)

rational :: BinaryAngle -> ExternalAngle
rational (BinaryAngle (Block pre pp) (Block per p))
  = ((pre `shiftL` p) - pre + per) % ((bit p - 1) `shiftL` pp)

rational2 :: (BinaryAngle, BinaryAngle) -> (ExternalAngle, ExternalAngle)
rational2 (lo, hi) = (rational lo, rational hi)

otherRep' :: BinaryAngle -> Maybe BinaryAngle
otherRep' (BinaryAngle (Block pre pp) (Block 0 1)) = Just $ binaryAngle (Block (pre - 1) pp) (Block 1 1)
otherRep' (BinaryAngle (Block pre pp) (Block 1 1)) = Just $ binaryAngle (Block (pre + 1) pp) (Block 0 1)
otherRep' _ = Nothing

otherRep :: BinaryAngle -> BinaryAngle
otherRep b = case otherRep' b of
  Just r -> r
  Nothing -> error $ show ("otherRep does not exist", b)
