module Mandelbrot.Symbolics.Block
  ( Block(..)
  , (!)
  , (!<)
  , concatMap
  , compact
  , rotate
  , splitAt
  , take
  , drop
  , toList
  , toListReversed
  , fromList
  , singleton
  , empty
  , append
  , concat
  ) where

import Prelude hiding
  ( concatMap
  , concat
  , splitAt
  , take
  , drop
  , (!!)
  )
import Data.Bits
  ( shiftL
  , shiftR
  , setBit
  , testBit
  , bit
  , (.&.)
  , (.|.)
  )
import Data.List
  ( foldl'
  )

data Block = Block !Integer !Int
  deriving (Eq, Read, Show)

empty :: Block
empty = Block 0 0

append :: Block -> Block -> Block
Block xs x `append` Block ys y = Block ((xs `shiftL` y) .|. ys) (x + y)

concat :: [Block] -> Block
concat = foldl' append empty

(!) :: Block -> Int -> Bool
Block b l ! i = b `testBit` (l - i - 1)

(!<) :: Block -> Int -> Bool
Block b _ !< i = b `testBit` i

concatMap :: (Bool -> Block) -> Block -> Block
concatMap f b@(Block _ l) = concat [ f (b ! i) | i <- [ 0 .. l - 1 ] ]

compact :: Block -> Block
compact b@(Block _ l) = head
  [ p
  | m <- [1 .. l]
  , l `mod` m == 0
  , let p = take m b
  , b == concat (replicate (l `div` m) p)
  ]

rotate :: Block -> Int -> Block
rotate (Block _ 0) _ = empty
rotate (Block b l) i = Block (((b `shiftL` j) .&. mask) .|. (b `shiftR` k)) l
  where
    j = i `mod` l
    k = l - i
    mask = bit l - 1

splitAt :: Int -> Block -> (Block, Block)
splitAt i (Block b l) = (Block (b `shiftR` m) i, Block (b .&. mask) m)
  where
    m = l - i
    mask = bit m - 1

take :: Int -> Block -> Block
take i = fst . splitAt i

drop :: Int -> Block -> Block
drop i = snd . splitAt i

toList :: Block -> [Bool]
toList b@(Block _ l) = [ b ! i | i <- [ 0 .. l - 1 ] ]

toListReversed :: Block -> [Bool]
toListReversed b@(Block _ l) = [ b !< i | i <- [ 0 .. l - 1 ] ]

fromList :: [Bool] -> Block
fromList = foldl' add empty
  where
    add (Block b l) True  = Block ((b `shiftL` 1) `setBit` 0) (l + 1)
    add (Block b l) False = Block ( b `shiftL` 1            ) (l + 1)

singleton :: Bool -> Block
singleton False = Block 0 1
singleton True  = Block 1 1
