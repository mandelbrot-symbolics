{-# LANGUAGE TypeFamilies #-}
{- |
Internal angles.
-}
module Mandelbrot.Symbolics.InternalAngle
  ( InternalAngle(..)
  ) where

import Prelude hiding
  ( Rational
  )
import Mandelbrot.Symbolics.Rational
  ( Q(..)
  , Rational
  )

newtype InternalAngle = InternalAngle Rational
  deriving (Read, Show, Eq, Ord)

instance Q InternalAngle where
  n % d = InternalAngle (n % d)
  n %! d = InternalAngle (n %! d)
  numerator (InternalAngle r) = numerator r
  denominator (InternalAngle r) = denominator r
