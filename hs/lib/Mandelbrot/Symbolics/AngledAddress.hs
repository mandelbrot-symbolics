module Mandelbrot.Symbolics.AngledAddress
  ( AngledAddress(..)
  , angledAddress'
  , angledAddress
  , addressAngles'
  , addressAngles
  , splitAddress
  , joinAddress
  , stripAddress
  , graftAddress
  ) where

import Control.Monad
  ( guard
  )
import Data.Bits
  ( shiftL
  , shiftR
  , (.&.)
  , (.|.)
  )
import Data.List
  ( elemIndex
  )

import Mandelbrot.Symbolics.ExternalAngle
  ( ExternalAngle
  , Tuning(..)
  )
import Mandelbrot.Symbolics.InternalAddress
  ( InternalAddress(..)
  , internalAddress'
  )
import Mandelbrot.Symbolics.InternalAngle
  ( InternalAngle
  )
import Mandelbrot.Symbolics.Kneading
  ( Kneading
  , kneading
  , unwrap
  )
import Mandelbrot.Symbolics.Period
  ( Period(preperiod, period)
  )
import Mandelbrot.Symbolics.Rational
  ( (%)
  , numerator
  , denominator
  , zero
  , one
  , wrap
  , double
  , doubleOdd
  )

data Pair a b = !a :!: !b

-- | Angled internal addresses have internal angles between each period in an
--   internal address.
data AngledAddress
  = Unangled !Int
  | Angled !Int !InternalAngle AngledAddress
  deriving (Read, Show, Eq, Ord)

-- | The period of an angled internal address.
instance Period AngledAddress where
  preperiod _ = 0
  period (Unangled p) = p
  period (Angled _ _ a) = period a

{-
-- | Builds a valid 'AngledAddress' from a list, checking the
--   precondition that only the last 'Maybe Angle' should be 'Nothing',
--   and the 'Integer' must be strictly increasing.
angledFromList :: [(Int, Maybe InternalAngle)] -> Maybe AngledAddress
angledFromList = fromList' 0
  where
    fromList' x [(n, Nothing)] | n > x = Just (Unangled n)
    fromList' x ((n, Just r) : xs) | n > x && zero < r && r < one = Angled n r `fmap` fromList' n xs
    fromList' _ _ = Nothing
-}

unsafeAngledFromList :: [(Int, Maybe InternalAngle)] -> AngledAddress
unsafeAngledFromList = fromList' 0
  where
    fromList' x [(n, Nothing)] | n > x = Unangled n
    fromList' x ((n, Just r) : xs) | n > x && zero < r && r < one = Angled n r (fromList' n xs)
    fromList' x xs = error $ "AngledAddress.unsafeAngledFromList " ++ show (x, xs)

-- | Convert an 'AngledAddress' to a list.
angledToList :: AngledAddress -> [(Int, Maybe InternalAngle)]
angledToList (Unangled n) = [(n, Nothing)]
angledToList (Angled n r a) = (n, Just r) : angledToList a

denominators :: InternalAddress -> Kneading -> [Int]
denominators (InternalAddress xs) v = denominators' xs
  where
    denominators' (s0:ss@(s1:_)) =
      let rr = r s0 s1
      in  (((s1 - rr) `div` s0) + if (s0 ==) . head . dropWhile (< s0) . iterate p $ rr then 1 else 2) : denominators' ss
    denominators' _ = []
    r s s' = case s' `mod` s of
      0 -> s
      t -> t
    p = rho v

rho :: Kneading -> Int -> Int
rho v = rho'
  where
    rho' r
      | r >= 1 && r `mod` n /= 0 = ((1 + r) +) . length . takeWhile id . zipWith (==) (unwrap v) . drop r $ (unwrap v)
      | otherwise = rho' (r + 1)
    n = period v

numerators :: ExternalAngle -> InternalAddress -> [Int] -> [Int]
numerators r (InternalAddress a) qs = zipWith num a qs
  where
    num s q = length . filter (<= r) . map (rs !!) $ [0 .. q - 2]
      where
        rs = iterate (\t -> foldr (.) id (replicate s (if even (denominator t) then double else doubleOdd)) $ t) (wrap r)

-- | The angled internal address corresponding to an external angle.
angledAddress' :: ExternalAngle -> Maybe AngledAddress
angledAddress' r0 = do
  let r = wrap r0
      k = kneading r
  i@(InternalAddress is) <- internalAddress' k
  let d = denominators i k
      n = numerators r i d
  return . unsafeAngledFromList . zip is . (++ [Nothing]) . map Just . zipWith (\a b -> fromIntegral a % fromIntegral b) n $ d

angledAddress :: ExternalAngle -> AngledAddress
angledAddress r = case angledAddress' r of
  Just a -> a
  Nothing -> error $ show ("angledAddress failed", r)

-- | Split an angled internal address at the last island.
splitAddress :: AngledAddress -> (AngledAddress, [InternalAngle])
splitAddress a =
  let (ps0, rs0) = unzip $ angledToList a
      ps1 = reverse ps0
      rs1 = reverse (Nothing : init rs0)
      prs1 = zip ps1 rs1
      f ((p, Just r):qrs@((q, _):_)) acc
        | p == fromIntegral (denominator r) * q = f qrs (r : acc)
      f prs acc = g prs acc
      g prs acc =
        let (ps2, rs2) = unzip prs
            ps3 = reverse ps2
            rs3 = reverse (Nothing : init rs2)
            prs3 = zip ps3 rs3
            aa = unsafeAngledFromList prs3
        in  (aa, acc)
  in  f prs1 []

-- | The inverse of 'splitAddress'.
joinAddress :: AngledAddress -> [InternalAngle] -> AngledAddress
joinAddress (Unangled p) [] = Unangled p
joinAddress (Unangled p) (r:rs) = Angled p r (joinAddress (Unangled $ p * fromIntegral (denominator r)) rs)
joinAddress (Angled p r a) rs = Angled p r (joinAddress a rs)

-- | Discard angle information from an internal address.
stripAddress :: AngledAddress -> InternalAddress
stripAddress = InternalAddress . map fst . angledToList


-- | The pair of external angles whose rays land at the root of the
--   hyperbolic component described by the angled internal address.
addressAngles' :: AngledAddress -> Maybe (ExternalAngle, ExternalAngle)
addressAngles' = externalAngles' 1 (zero, one)

addressAngles :: AngledAddress -> (ExternalAngle, ExternalAngle)
addressAngles a = case addressAngles' a of
  Just lh -> lh
  Nothing -> error $ show ("addressAngles failed", a)

externalAngles' :: Int -> (ExternalAngle, ExternalAngle) -> AngledAddress -> Maybe (ExternalAngle, ExternalAngle)
externalAngles' p0 lohi a0@(Unangled p)
  | p0 /= p = case wakees lohi p of
      [lh] -> externalAngles' p lh a0
      _ -> Nothing
  | otherwise = Just lohi
externalAngles' p0 lohi a0@(Angled p r a)
  | p0 /= p = case wakees lohi p of
      [lh] -> externalAngles' p lh a0
      _ -> Nothing
  | otherwise = do
      let num = numerator r
          den = denominator r
          ws = wakees (zero, one) (fromIntegral den)
          nums = [ num' | num' <- [ 1.. den - 1 ], let r' = num' % den :: ExternalAngle, denominator r' == den ]
          nws, nnums :: Int
          nws = length ws
          nnums = length nums
      guard (nws == nnums)
      i <- elemIndex num nums
      (l,h) <- safeIndex ws i
      externalAngles' (p * fromIntegral den) (if p > 1 then (tune lohi l, tune lohi h) else (l, h)) a

wakees :: (ExternalAngle, ExternalAngle) -> Int -> [(ExternalAngle, ExternalAngle)]
wakees (lo, hi) q =
  let gaps (l :!: h) n
        | n == 0 = [(l :!: h)]
        | n > 0 = let gs = gaps (l :!: h) (n - 1)
                      cs = candidates n gs
                  in  accumulate cs gs
        | otherwise = error $ "AngledAddress.gaps: !(n >= 0) " ++ show n
      candidates n gs =
        let den = (1 `shiftL` n) - 1
        in  [ r
            | (l :!: h) <- gs
            , num <- [ ceiling' l n .. floor' h n ]
            , fullperiod n num
            , let r = num % den
            , l < r, r < h
            ]
      accumulate [] ws = ws
      accumulate (l : h : lhs) ws =
        let (ls, ms@((ml :!: _):_)) = break (l `inside`) ws
            (_s, (_ :!: rh):rs) = break (h `inside`) ms
        in  ls ++ [(ml :!: l)] ++ accumulate lhs ((h :!: rh) : rs)
      accumulate _ _ = error "AngledAddress.accumulate !even"
      inside x (l :!: h) = l < x && x < h
      fullperiod bs = \n -> and [ (((n `shiftR` b) .|. (n `shiftL` (bs - b))) .&. mask) /= n | b <- factors ]
        where
          factors = [ b | b <- [ bs - 1, bs - 2 .. 1 ], bs `mod` b == 0 ]
          mask = (1 `shiftL` bs) - 1
  in  chunk2 . candidates q . gaps (lo :!: hi) $ (q - 1)

chunk2 :: [t] -> [(t, t)]
chunk2 [] = []
chunk2 (x:y:zs) = (x, y) : chunk2 zs
chunk2 _ = error "AngledAddress.chunk2 !even"

safeIndex :: [a] -> Int -> Maybe a
safeIndex [] _ = Nothing
safeIndex (x:xs) i
  | i < 0 = Nothing
  | i > 0 = safeIndex xs (i - 1)
  | otherwise = Just x

-- | ceiling' x y = ceiling $ x * (2^y - 1)
ceiling' :: ExternalAngle -> Int -> Integer
ceiling' x y = ((numerator x `shiftL` y) - numerator x + denominator x - 1) `div` denominator x

-- | floor' x y = floor $ x * (2^y - 1)
floor' :: ExternalAngle -> Int -> Integer
floor' x y = ((numerator x `shiftL` y) - numerator x) `div` denominator x

graftAddress :: AngledAddress -> AngledAddress -> AngledAddress
graftAddress r@(Unangled p) a@(Unangled q)
  | p == q = a
  | otherwise = r
graftAddress (Angled p t r) a@(Unangled q)
  | p == q = a
  | otherwise = Angled p t (graftAddress r a)
graftAddress r@(Unangled p) a@(Angled q _ _)
  | p == q = a
  | otherwise = r
graftAddress (Angled p t r) a@(Angled q _ _)
  | p == q = a
  | otherwise = Angled p t (graftAddress r a)
