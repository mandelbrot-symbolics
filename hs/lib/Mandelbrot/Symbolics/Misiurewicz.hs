module Mandelbrot.Symbolics.Misiurewicz
  ( angleCount
  , externalAngles
  ) where

import Prelude hiding
  ( concat
  , splitAt
  , take
  )
import qualified Prelude as P

import Data.List
  ( nub
  , sort
  )
import Data.Maybe
  ( mapMaybe
  )

import Mandelbrot.Symbolics.AngledAddress
  ( angledAddress'
  , addressAngles'
  )
import Mandelbrot.Symbolics.Block
  ( Block(..)
  , splitAt
  , take
  , append
  , concat
  )
import Mandelbrot.Symbolics.ExternalAngle
  ( ExternalAngle(..)
  , BinaryAngle(..)
  , binaryAngle
  , rational
  , binary
  )
import Mandelbrot.Symbolics.Kneading
  ( Kneading(PrePeriodic)
  , kneading
  )
import Mandelbrot.Symbolics.Period
  ( Period(periods, period)
  )

angleCount :: ExternalAngle -> Maybe Int
angleCount r
  | pp == 0   = Just 2
  | q  == 1   = Nothing -- either 1 or 2
  | otherwise = Just q
  where
    (pp, p) = periods r
    q = p `div` period (kneading r)

externalAngles :: BinaryAngle -> [BinaryAngle]
externalAngles = rays
  where
    periodic :: Int -> BinaryAngle -> Maybe BinaryAngle
    periodic = preperiodic 0
    preperiodic :: Int -> Int -> BinaryAngle -> Maybe BinaryAngle
    preperiodic preperiod' period' (BinaryAngle pre@(Block _ pp) per@(Block _ p)) =
      let n = preperiod' + period' - pp
          k = (n + p - 1) `div` p  -- ceiling (n / p)
          (pre', per') = splitAt preperiod' (pre `append` concat (replicate k per))
      in  check $ binaryAngle pre' (take period' per')
      where
        check t@(BinaryAngle (Block _ pp') (Block _ p'))
          | pp' == preperiod' && p' == period' = Just t
          | otherwise = Nothing
    rays :: BinaryAngle -> [BinaryAngle]
    rays t
      | pp == 0 && p == 1 = [BinaryAngle (Block 0 0) (Block 0 1), BinaryAngle (Block 0 0) (Block 1 1)]
      | pp == 0 = case fmap (map binary . sort . (\(a,b) -> [a,b])) . (addressAngles' =<<) . angledAddress' . rational $ t of
          Just xs -> xs
          Nothing -> []
      | pp > 0 = case kneading (rational t) of
          PrePeriodic _ (Block _ kp) -> case p `divMod` kp of
            (n, m)
              | m /= 0 -> error $ "rays Preperiodic: " ++ show ((pp, p), kp, n, m, t)
              | n > 1 -> case dropWhile ((n /=) . length) . iterate (rays' n pp p) $ [t] of
                  h:_ -> h
                  [] -> error $ "Misiurewicz.rays: [] " ++ show ((pp, p), kp, n, m, t)
              | n == 1 -> rays'' pp p t
              | otherwise -> error $ "Misiureiwicz.rays: n<1 " ++ show ((pp, p), kp, n, m, t)
          k -> error $ "Misiurewicz.rays: !pp " ++ show (pp, p, t, k)
      | otherwise = error $ "Misiurewicz.rays: pp<0 " ++ show (pp, p, t)
      where
        (pp, p) = periods t
    rays' :: Int -> Int -> Int ->  [BinaryAngle] -> [BinaryAngle]
    rays' n pp p ts
      | not (null ts)
        = map binary
        . sort
        . P.take (n `min` (length ts + 2))
        . nub
        . map rational
        . mapMaybe (preperiodic pp p)
        . P.concat
        . mapMaybe
            ( fmap (map binary . (\(a,b) -> [a,b]))
            . (addressAngles' =<<)
            . (angledAddress' =<<)
            . fmap rational
            )
        $ [ periodic m t | m <- [2 * pp + p ..], t <- ts ]
      | otherwise = error "Misiurewicz.rays': null ts"
    rays'' :: Int -> Int -> BinaryAngle -> [BinaryAngle]
    rays'' pp p t
      = map binary
      . sort
      . nub
      . map rational
      . mapMaybe (preperiodic pp p)
      . P.concat
      . mapMaybe
          ( fmap (map binary . (\(a,b) -> [a,b]))
          . (addressAngles' =<<)
          . (angledAddress' =<<)
          . fmap rational
          )
      $ [ periodic m t | m <- [2 * (pp + p) .. 3 * (pp + p)] ]
