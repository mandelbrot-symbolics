module Mandelbrot.Symbolics.Period
  ( Period(..)
  , Periods(..)
  ) where

class Period t where
  preperiod :: t -> Int
  preperiod = fst . periods
  period :: t -> Int
  period = snd . periods
  periods :: t -> (Int, Int)
  periods t = (preperiod t, period t)
  safePeriods :: Int -> t -> Maybe (Int, Int)
  safePeriods _ = Just . periods

newtype Periods = Periods (Int, Int)
  deriving (Eq, Read, Show)

instance Period Periods where
  periods (Periods p) = p
