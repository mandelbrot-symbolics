{- |
Kneading.
-}
module Mandelbrot.Symbolics.Kneading
  ( Kneading(..)
  , kneading
  , unwrap
  ) where

import Prelude hiding
  ( Rational
  )
import Data.Maybe
  ( catMaybes
  )

import Mandelbrot.Symbolics.Block
  ( Block(..)
  , compact
  , toList
  , fromList
  , empty
  )
import Mandelbrot.Symbolics.ExternalAngle
  ( ExternalAngle
  )
import Mandelbrot.Symbolics.Period
  ( Period(periods)
  )
import Mandelbrot.Symbolics.Rational
  ( denominator
  , wrap
  , double
  , doubleOdd
  , preimages
  , zero
  )

-- | Kneading sequences.
data Kneading
  = PrePeriodic !Block !Block
  | StarPeriodic !Block -- shorter by one bit, with implicit final Star
  | Periodic !Block
  deriving (Read, Show, Eq)

instance Period Kneading where
  periods (PrePeriodic (Block _ pp) (Block _ p)) = (pp, p)
  periods (StarPeriodic (Block _ p)) = (0, p + 1)
  periods (Periodic (Block _ p)) = (0, p)

-- | The kneading sequence for an external angle.
kneading :: ExternalAngle -> Kneading
kneading a0'
  | a0 == zero = StarPeriodic empty
  | otherwise = case span (even . denominator . fst) . kneading' $ a0 of
      (pre, ak1@(a1,_):aks) -> case takeWhile ((a1 /=) . fst) aks of
        aks' ->
          let per = map snd $ ak1 : aks'
          in  case (null pre, last per) of
            (True, Nothing) -> StarPeriodic (fromList (catMaybes per))
            (True, _) -> Periodic (compact (fromList (catMaybes per)))
            (False, _) -> PrePeriodic (fromList (catMaybes (map snd pre))) (compact (fromList (catMaybes per)))
      ppp -> error $ "kneading: " ++ show (a0', ppp)
  where
    a0 = wrap a0'
    (lo, hi) = preimages a0
    kneading' a
      | even (denominator a) = (a, knead a) : kneading' (double a)
      | otherwise = kneading'' a
    kneading'' a = (a, knead a) : kneading'' (doubleOdd a)
    knead a
      | a == lo          = Nothing
      | a == hi          = Nothing
      | lo < a && a < hi = Just True
      | hi < a || a < lo = Just False
      | otherwise = error $ "kneading.knead: " ++ show (a, lo, hi)

unwrap :: Kneading -> [Maybe Bool]
unwrap (PrePeriodic pre per) = map Just (toList pre) ++ cycle (map Just (toList per))
unwrap (StarPeriodic per) = cycle (map Just (toList per) ++ [Nothing])
unwrap (Periodic per) = cycle (map Just (toList per))
