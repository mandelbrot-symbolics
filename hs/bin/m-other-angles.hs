import Mandelbrot.Symbolics hiding (concatMap)
import Mandelbrot.Text (parse, plain)

main' :: String -> [String]
main' a = case binary2 . addressAngles . angledAddress . rational . parse $ a of
  (lo, hi) -> map plain [lo, hi]

main :: IO ()
main = interact (unlines . concatMap main' . lines)
