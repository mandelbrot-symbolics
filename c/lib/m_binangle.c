#include <mandelbrot-symbolics.h>
#include <assert.h>
#include <stdlib.h>

extern void m_binangle_init(m_binangle *a) {
  m_block_init(&a->pre);
  m_block_init(&a->per);
  a->per.length = 1;
}

extern void m_binangle_clear(m_binangle *a) {
  m_block_clear(&a->pre);
  m_block_clear(&a->per);
}

extern void m_binangle_from_rational(m_binangle *a, const mpq_t q) {
  mpq_t p;
  mpq_init(p);
  a->pre.length = m_preperiod(q);
  mpz_fdiv_q_2exp(mpq_denref(p), mpq_denref(q), a->pre.length);
  mpz_fdiv_qr(a->pre.bits, mpq_numref(p), mpq_numref(q), mpq_denref(p));
  a->per.length = m_period(p);
  mpz_mul_2exp(a->per.bits, mpq_numref(p), a->per.length);
  mpz_sub(a->per.bits, a->per.bits, mpq_numref(p));
  mpz_fdiv_q(a->per.bits, a->per.bits, mpq_denref(p));
  mpq_clear(p);
}

extern void m_binangle_to_rational(mpq_t q, const m_binangle *a) {
  mpz_mul_2exp(mpq_numref(q), a->pre.bits, a->per.length);
  mpz_sub(mpq_numref(q), mpq_numref(q), a->pre.bits);
  mpz_add(mpq_numref(q), mpq_numref(q), a->per.bits);
  mpz_set_si(mpq_denref(q), 0);
  mpz_setbit(mpq_denref(q), a->per.length);
  mpz_sub_ui(mpq_denref(q), mpq_denref(q), 1);
  mpz_mul_2exp(mpq_denref(q), mpq_denref(q), a->pre.length);
  mpq_canonicalize(q);
}

extern void m_binangle_canonicalize(m_binangle *a)
{
  mpq_t q;
  mpq_init(q);
  m_binangle_to_rational(q, a);
  m_binangle_from_rational(a, q);
  mpq_clear(q);
}

extern int m_binangle_strlen(const m_binangle *a) {
  return 4 + a->pre.length + a->per.length;
}

extern void m_binangle_to_string(char *s, const m_binangle *a) {
  int k = 0;
  s[k++] = '.';
  m_block_to_string(s + k, &a->pre);
  k += a->pre.length;
  s[k++] = '(';
  m_block_to_string(s + k, &a->per);
  k += a->per.length;
  s[k++] = ')';
  s[k] = 0;
}

extern const char *m_binangle_from_string(m_binangle *a, const char *s) {
  const char *t = s;
  if (*t != '.') return 0;
  t = m_block_from_string(&a->pre, t + 1);
  if (*t != '(') return 0;
  t = m_block_from_string(&a->per, t + 1);
  if (*t != ')') return 0;
  if (a->per.length <= 0) return 0;
  return t + 1;
}

extern void m_binangle_tune(m_binangle *o, const m_binangle *i, const m_block *lo, const m_block *hi) {
  m_block_concatmap(&o->pre, &i->pre, lo, hi);
  m_block_concatmap(&o->per, &i->per, lo, hi);
}

extern void m_binangle_set(m_binangle *o, const m_binangle *a)
{
  m_block_set(&o->pre, &a->pre);
  m_block_set(&o->per, &a->per);
}

extern char *m_binangle_to_new_string(const m_binangle *a)
{
  int bytes = m_binangle_strlen(a);
  char *o = malloc(bytes + 1);
  m_binangle_to_string(o, a);
  return o;
}

extern bool m_binangle_other_representation(m_binangle *a)
{
  if (a->per.length == 1)
  {
    int b = mpz_get_ui(a->per.bits) & 1;
    mpz_set_ui(a->per.bits, ! b);
    if (a->pre.length > 0)
    {
      if (b)
      {
        mpz_add_ui(a->pre.bits, a->pre.bits, 1);
      }
      else
      {
        mpz_sub_ui(a->pre.bits, a->pre.bits, 1);
      }
    }
    return true;
  }
  return false;
}

extern void m_binangle_fprint(FILE *out, const m_binangle *a)
{
  char *str = malloc(m_binangle_strlen(a) + 1);
  m_binangle_to_string(str, a);
  fputs(str, out);
  free(str);
}

extern void m_binangle_print(const m_binangle *a)
{
  m_binangle_fprint(stdout, a);
}

extern bool m_binangle_test_bit(const m_binangle *a, int b)
{
  if (b < a->pre.length)
  {
    return mpz_tstbit(a->pre.bits, a->pre.length - 1 - b);
  }
  b -= a->pre.length;
  b %= a->per.length;
  return mpz_tstbit(a->per.bits, a->per.length - 1 - b);
}

extern void m_binangle_tip(m_binangle *tip, const m_binangle *lo, const m_binangle *hi)
{
  mpz_set_ui(tip->pre.bits, 0); tip->pre.length = 0;
  mpz_set_ui(tip->per.bits, 0); tip->per.length = 1;
  // longest matching prefix with 1(0) appended
  m_block o, i;
  m_block_init(&o);
  m_block_init(&i);
  mpz_set_ui(o.bits, 0); o.length = 1;
  mpz_set_ui(i.bits, 1); i.length = 1;
  bool running = true;
  for (int b = 0; running; ++b)
  {
    bool x = m_binangle_test_bit(lo, b);
    bool y = m_binangle_test_bit(hi, b);
    if (x == y)
    {
      m_block_append(&tip->pre, &tip->pre, x ? &i : &o);
    }
    else
    {
      running = false;
    }
  }
  m_block_append(&tip->pre, &tip->pre, &i);
  m_block_clear(&o);
  m_block_clear(&i);
}
