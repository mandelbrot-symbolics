#include <mandelbrot-symbolics.h>

extern void m_block_init(m_block *b) {
  mpz_init(b->bits);
  b->length = 0;
}

extern void m_block_clear(m_block *b) {
  mpz_clear(b->bits);
}

extern void m_block_empty(m_block *b) {
  mpz_set_si(b->bits, 0);
  b->length = 0;
}

extern void m_block_append(m_block *o, const m_block *l, const m_block *r) {
  if (o == r) {
    mpz_t rbits;
    mpz_init(rbits);
    mpz_set(rbits, r->bits);
    mpz_mul_2exp(o->bits, l->bits, r->length);
    mpz_ior(o->bits, o->bits, rbits);
    o->length = l->length + r->length;
    mpz_clear(rbits);
  } else {
    mpz_mul_2exp(o->bits, l->bits, r->length);
    mpz_ior(o->bits, o->bits, r->bits);
    o->length = l->length + r->length;
  }
}

extern void m_block_concatmap(m_block *o, const m_block *i, const m_block *lo, const m_block *hi) {
  if (o == i || o == lo || o == hi) {
    m_block o2;
    m_block_init(&o2);
    m_block_empty(&o2);
    for (int k = 0; k < i->length; ++k) {
      m_block_append(&o2, &o2, mpz_tstbit(i->bits, i->length - 1 - k) ? hi : lo);
    }
    mpz_set(o->bits, o2.bits);
    o->length = o2.length;
    m_block_clear(&o2);
  } else {
    m_block_empty(o);
    for (int k = 0; k < i->length; ++k) {
      m_block_append(o, o, mpz_tstbit(i->bits, i->length - 1 - k) ? hi : lo);
    }
  }
}

extern const char *m_block_from_string(m_block *b, const char *s) {
  mpz_set_si(b->bits, 0);
  int i;
  for (i = 0; s[i] == '0' || s[i] == '1'; ++i) {
    mpz_mul_2exp(b->bits, b->bits, 1);
    if (s[i] == '1') {
      mpz_setbit(b->bits, 0);
    }
  }
  b->length = i;
  return s + i;
}

extern void m_block_to_string(char *s, const m_block *b) {
  for (int i = 0; i < b->length; ++i) {
    s[i] = '0' + mpz_tstbit(b->bits, b->length - 1 - i);
  }
  s[b->length] = 0;
}

extern void m_block_set(m_block *o, const m_block *a)
{
  mpz_set(o->bits, a->bits);
  o->length = a->length;
}
