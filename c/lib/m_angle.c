#include <mandelbrot-symbolics.h>
#include <assert.h>

extern int m_preperiod(const mpq_t q)
{
  return mpz_scan1(mpq_denref(q), 0);
}

extern int m_period(const mpq_t angle0)
{
  // angle must be canonicalized
  // angle must be strictly periodic
  assert(0 == m_preperiod(angle0));
  // angle must be in [0..1)
  assert(0 <= mpz_sgn(mpq_numref(angle0)));
  assert(mpz_cmp(mpq_numref(angle0), mpq_denref(angle0)) < 0);
  mpq_t angle;
  mpq_init(angle);
  mpq_set(angle, angle0);
  int period = 0;
  do
  {
    mpq_mul_2exp(angle, angle, 1);
    if (mpz_cmp(mpq_numref(angle), mpq_denref(angle)) >= 0)
    {
      mpz_sub(mpq_numref(angle), mpq_numref(angle), mpq_denref(angle));
    }
    period++;
  } while (! mpq_equal(angle, angle0));
  mpq_clear(angle);
  return period;
}
