/*

Ref:

Symbolic Dynamics of Quadratic Polynomials
Henk Bruin and Dierk Schleicher

Algorithm 13.3

Given an external angle v of exact period n >= 2, the
conjugate angle v' can be found as follows: let v_1
be the preperiodic preimage of v, and for k = 2, 3,
..., n let v_k be the unique preimage of v_{k-1} such
that (2^{n-k} v, v_k) and (2^{n-1} v, v_1) are
unlinked; then

v' = v + \frac{v_n - v}{1 - 2^{-n}}
   = v_n + 2^{-n} \frac{v_n - v}{1 - 2^{-n}}

*/

#include <mandelbrot-symbolics.h>
#include <assert.h>

extern void m_q_conjugate(mpq_t q_out, const mpq_t q)
{
  m_binangle vk, v2nk;
  m_binangle_init(&vk);
  m_binangle_init(&v2nk);
  mpq_t q1, q2n1, qk, q2nk;
  mpq_init(q1);
  mpq_init(q2n1);
  mpq_init(qk);
  mpq_init(q2nk);
  m_binangle_from_rational(&vk, q);
  m_binangle_set(&v2nk, &vk);
  int n = vk.per.length;
  assert(vk.pre.length == 0);
  assert(vk.per.length >= 2);
  // preperiodic preimage
  vk.pre.length = 1;
  mpz_set_ui(vk.pre.bits, ! mpz_tstbit(vk.per.bits, 0));
  m_binangle_to_rational(q1, &vk);
  // periodic preimage
  if (mpz_tstbit(v2nk.per.bits, 0))
  {
    mpz_setbit(v2nk.per.bits, v2nk.per.length);
  }
  mpz_div_2exp(v2nk.per.bits, v2nk.per.bits, 1);
  m_binangle_to_rational(q2n1, &v2nk);
  for (int k = 2; k <= n; ++k)
  {
    // periodic preimage
    if (mpz_tstbit(v2nk.per.bits, 0))
    {
      mpz_setbit(v2nk.per.bits, v2nk.per.length);
    }
    mpz_div_2exp(v2nk.per.bits, v2nk.per.bits, 1);
    // choose unlinked preimage
    m_binangle_to_rational(q2nk, &v2nk);
    vk.pre.length = k;
    m_binangle_to_rational(qk, &vk);
    if (! m_q_unlinked(q2nk, qk, q2n1, q1))
    {
      mpz_setbit(vk.pre.bits, k - 1);
      m_binangle_to_rational(qk, &vk);
    }
    assert(m_q_unlinked(q2nk, qk, q2n1, q1));
  }
  // v_n + \frac{v_n - v}{2^n - 1}
  mpq_sub(q_out, qk, q);
  mpz_set_si(mpq_numref(q2n1), 1);
  mpz_set_si(mpq_denref(q2n1), 0);
  mpz_setbit(mpq_denref(q2n1), n);
  mpz_sub_ui(mpq_denref(q2n1), mpq_denref(q2n1), 1);
  mpq_mul(q_out, q_out, q2n1);
  mpq_add(q_out, q_out, qk);
  // clean up
  m_binangle_clear(&vk);
  m_binangle_clear(&v2nk);
  mpq_clear(q1);
  mpq_clear(q2n1);
  mpq_clear(qk);
  mpq_clear(q2nk);
}
