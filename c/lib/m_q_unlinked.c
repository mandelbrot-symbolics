/* We say that two pairs of different angles (a, a') and (b, b')
are unlinked if {b,b'} is in a single connected component of
S^1\{a,a'} (or equivalently if {a,a'} is in a single connected
component of S^1\{b,b'}). */

#include <mandelbrot-symbolics.h>

extern bool m_q_unlinked(const mpq_t a1, const mpq_t a2, const mpq_t b1, const mpq_t b2)
{
  if (0 > mpq_cmp(a1, a2))
  {
    if (0 > mpq_cmp(b1, b2))
    {
      if (0 > mpq_cmp(a1, b1))
      {
        return 0 > mpq_cmp(a2, b1) || 0 > mpq_cmp(b2, a2);
      }
      else
      {
        return 0 > mpq_cmp(b2, a1) || 0 > mpq_cmp(a2, b2);
      }
    }
    else
    {
      if (0 > mpq_cmp(a1, b2))
      {
        return 0 > mpq_cmp(a2, b2) || 0 > mpq_cmp(b1, a2);
      }
      else
      {
        return 0 > mpq_cmp(b1, a1) || 0 > mpq_cmp(a2, b1);
      }
    }
  }
  else
  {
    if (0 > mpq_cmp(b1, b2))
    {
      if (0 > mpq_cmp(a2, b1))
      {
        return 0 > mpq_cmp(a1, b1) || 0 > mpq_cmp(b2, a1);
      }
      else
      {
        return 0 > mpq_cmp(b2, a2) || 0 > mpq_cmp(a1, b2);
      }
    }
    else
    {
      if (0 > mpq_cmp(a2, b2))
      {
        return 0 > mpq_cmp(a1, b2) || 0 > mpq_cmp(b1, a1);
      }
      else
      {
        return 0 > mpq_cmp(b1, a2) || 0 > mpq_cmp(a1, b1);
      }
    }
  }
}
