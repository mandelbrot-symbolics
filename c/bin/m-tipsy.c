#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <mandelbrot-symbolics.h>

extern int main(int argc, char **argv)
{
  if (! (argc > 3))
  {
    return 1;
  }
  // external angle pair
  m_binangle o, O;
  m_binangle_init(&o);
  m_binangle_init(&O);
  m_binangle_from_string(&o, argv[1]);
  m_binangle_from_string(&O, argv[2]);
  // external angle of tip
  m_binangle s;
  m_binangle_init(&s);
  m_binangle_from_string(&s, argv[3]);
  assert(s.per.length == 1);
  assert(mpz_get_ui(s.per.bits) == 0);
  // base of filament
  m_binangle t, T;
  m_binangle_init(&t);
  m_binangle_init(&T);
  m_binangle_tune(&T, &s, &o.per, &O.per);
  m_binangle_print(&s); putchar('\n');
  m_binangle_other_representation(&s);
  m_binangle_print(&s); putchar('\n');
  m_binangle_tune(&t, &s, &o.per, &O.per);
  m_binangle_print(&t); putchar('\n');
  m_binangle_print(&T); putchar('\n');
  // tip of filament
  //m_binangle_tip(&S, &t, &T);
  // width of wake
  m_binangle w;
  m_binangle_init(&w);
  mpq_t q, Q, Qq;
  mpq_init(q);
  mpq_init(Q);
  mpq_init(Qq);
  m_binangle_to_rational(q, &t);
  m_binangle_to_rational(Q, &T);
  mpq_sub(Qq, Q, q);
  m_binangle_from_rational(&w, Qq);
  mpq_clear(Qq);
  m_binangle_print(&w); putchar('\n');
  // lowest period in filament is index of first 1 in width of wake
  int Pj;
  for (int p = 0; true; ++p)
  {
    if (m_binangle_test_bit(&w, p))
    {
      Pj = p + 1;
      break;
    }
  }
  m_binangle j, J;
  m_binangle_init(&j);
  m_binangle_init(&J);
  // P21 = 2^P - 1
  mpz_t P21;
  mpz_init(P21);
  mpz_set_ui(P21, 0);
  mpz_setbit(P21, Pj);
  mpz_sub_ui(P21, P21, 1);
  // period p island in the filament has ray pair
  //  (ceil[(2^p-1)(q)]/(2^p-1),
  //  floor[(2^p-1)(Q)]/(2^p-1))
  mpq_t k, K;
  mpq_init(k);
  mpq_init(K);
  mpz_mul(mpq_numref(k), mpq_numref(q), P21);
  mpz_set(mpq_denref(k), mpq_denref(q));
  mpq_canonicalize(k);
  mpz_mul(mpq_numref(K), mpq_numref(Q), P21);
  mpz_set(mpq_denref(K), mpq_denref(Q));
  mpq_canonicalize(K);
  mpz_cdiv_q(j.per.bits, mpq_numref(k), mpq_denref(k));
  mpz_fdiv_q(J.per.bits, mpq_numref(K), mpq_denref(K));
  j.per.length = Pj;
  J.per.length = Pj;
  mpq_clear(k);
  mpq_clear(K);
  mpz_clear(P21);
  // (o, O) is the outer island
  // (j, J) is the inner island
  // embedded Julia set has ends .j(o) .j(O) .J(o) . J(O) (possibly not canonical!)
  // nucleus towards ends is inner (.(jo),.(JO)), outer (.(jO),.(Jo))
  m_binangle jo, JO;
  m_binangle_init(&jo);
  m_binangle_init(&JO);
  m_binangle_print(&o); putchar('\n');
  m_binangle_print(&O); putchar('\n');
  m_binangle_print(&j); putchar('\n');
  m_binangle_print(&J); putchar('\n');
  for (int depth = 0; depth < 10; ++depth)
  {
    // (o, O) ->
    // (j, J) ->
    // (j O, J o) ->
    // (jO J, Jo j) ->
    // (jOJ Jo, Joj jO) ->
    // ...
    m_block_append(&jo.per, &j.per, &O.per);
    m_block_append(&JO.per, &J.per, &o.per);
    m_binangle_print(&jo); putchar('\n');
    m_binangle_print(&JO); putchar('\n');
    m_binangle_set(&o, &j);
    m_binangle_set(&O, &J);
    m_binangle_set(&j, &jo);
    m_binangle_set(&J, &JO);
  }
  printf("%d ", j.per.length); m_binangle_print(&j); putchar('\n');
  // FIXME cleanup?
  return 0;
}
