#include <stdio.h>
#include <stdlib.h>
#include <mandelbrot-symbolics.h>

int main(int argc, char **argv) {
  if (! (argc > 1)) {
    return 1;
  }
  mpq_t q;
  mpq_init(q);
  mpq_set_str(q, argv[1], 10);
  mpq_canonicalize(q);
  m_binangle ba;
  m_binangle_init(&ba);
  m_binangle_from_rational(&ba, q);
  char *s = malloc(m_binangle_strlen(&ba));
  m_binangle_to_string(s, &ba);
  printf("%s\n", s);
  free(s);
  m_binangle_clear(&ba);
  mpq_clear(q);
  return 0;
}
