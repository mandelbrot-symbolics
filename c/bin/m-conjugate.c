#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mandelbrot-symbolics.h>

int main(int argc, char **argv)
{
  m_binangle ba;
  m_binangle_init(&ba);
  if (argc > 1)
  {
    m_binangle_from_string(&ba, argv[1]);
  }
  else
  {
    char *line = 0;
    size_t n;
    getline(&line, &n, stdin);
    char *eol;
    if ((eol = strrchr(line, '\n')))
    {
      *eol = 0;
    }
    m_binangle_from_string(&ba, line);
    free(line);
  }
  mpq_t q, q_out;
  mpq_init(q);
  mpq_init(q_out);
  m_binangle_to_rational(q, &ba);
  m_q_conjugate(q_out, q);
  m_binangle_from_rational(&ba, q_out);
  char *s = malloc(m_binangle_strlen(&ba));
  m_binangle_to_string(s, &ba);
  printf("%s\n", s);
  free(s);
  mpq_clear(q_out);
  mpq_clear(q);
  m_binangle_clear(&ba);
  return 0;
}
