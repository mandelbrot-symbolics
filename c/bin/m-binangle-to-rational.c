#include <stdio.h>
#include <stdlib.h>
#include <mandelbrot-symbolics.h>

int main(int argc, char **argv) {
  if (! (argc > 1)) {
    return 1;
  }
  m_binangle ba;
  m_binangle_init(&ba);
  m_binangle_from_string(&ba, argv[1]);
  mpq_t q;
  mpq_init(q);
  m_binangle_to_rational(q, &ba);
  m_binangle_clear(&ba);
  mpq_out_str(stdout, 10, q);
  mpq_clear(q);
  putchar('\n');
  return 0;
}
