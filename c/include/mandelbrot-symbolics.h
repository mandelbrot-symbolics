#ifndef MANDELBROT_SYMBOLICS_H
#define MANDELBROT_SYMBOLICS_H 1

#include <stdbool.h>
#include <stdio.h>
#include <gmp.h>

extern int m_preperiod(const mpq_t angle);
extern int m_period(const mpq_t angle);
extern bool m_q_unlinked(const mpq_t a1, const mpq_t a2, const mpq_t b1, const mpq_t b2);
extern void m_q_conjugate(mpq_t q_out, const mpq_t q);

struct m_block {
  mpz_t bits;
  int length;
};
typedef struct m_block m_block;

extern void m_block_init(m_block *b);
extern void m_block_clear(m_block *b);
extern void m_block_set(m_block *o, const m_block *a);
extern void m_block_empty(m_block *b);
extern void m_block_append(m_block *o, const m_block *l, const m_block *r);
extern void m_block_concatmap(m_block *o, const m_block *i, const m_block *lo, const m_block *hi);
extern const char *m_block_from_string(m_block *b, const char *s);
extern void m_block_to_string(char *s, const m_block *b);

struct m_binangle {
  m_block pre;
  m_block per;
};
typedef struct m_binangle m_binangle;

extern void m_binangle_init(m_binangle *a);
extern void m_binangle_clear(m_binangle *a);
extern void m_binangle_set(m_binangle *o, const m_binangle *a);
extern void m_binangle_from_rational(m_binangle *a, const mpq_t q);
extern void m_binangle_to_rational(mpq_t q, const m_binangle *a);
extern const char *m_binangle_from_string(m_binangle *a, const char *s);
extern int m_binangle_strlen(const m_binangle *a);
extern void m_binangle_to_string(char *s, const m_binangle *a);
extern char *m_binangle_to_new_string(const m_binangle *a);
extern void m_binangle_canonicalize(m_binangle *a);
extern bool m_binangle_is_canonical(const m_binangle *a);
extern bool m_binangle_other_representation(m_binangle *a);
extern void m_binangle_tune(m_binangle *o, const m_binangle *i, const m_block *lo, const m_block *hi);
extern void m_binangle_bulb(m_binangle *lo, m_binangle *hi, const mpq_t q);
extern void m_binangle_tip(m_binangle *tip, const m_binangle *lo, const m_binangle *hi);
extern void m_binangle_fprint(FILE *out, const m_binangle *a);
extern void m_binangle_print(const m_binangle *a);
extern bool m_binangle_test_bit(const m_binangle *a, int b);

#endif
